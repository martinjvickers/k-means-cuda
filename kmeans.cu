// A k-means clustering algorithm for cuda cards
// This program outputs the clusters, not the assignments
//
//
// Written by Martin Vickers
// mjv08@aber.ac.uk
// 
// usage:
// ./kmeans_cuda filename kval
// e.g.
// ./kmeans_cuda points.txt kval
//
//
 
#include <stdio.h>
#include <string.h>
#include <cuda.h>
#include <stdlib.h>

#define BLOCK_SIZE 512

__global__ void assign(float *points, float *centroids, int *assign, int k, int cols, int rows){
	
	float smdist;

	int i = blockIdx.x * blockDim.x + threadIdx.x;

	if(i < rows){

		for(int c = 0; c < k; c++){

			float square = 0;

			//Euclidean Distance for n-D points
			for(int d = 0; d < cols; d++){
				square = square + ((points[(i*cols)+d] - centroids[(c*cols)+d]) * (points[(i*cols)+d] - centroids[(c*cols)+d])) ;
			}

			float edist = sqrt(square);
	
			if(c==0){
				smdist=edist;
				assign[i]=c;
			} else if(edist < smdist){
				smdist=edist;
                	        assign[i]=c;
			}

		}	

	}

}

__global__ void ave(float *points, float *centroids, int *assign, int k, int cols, int rows){

        int i = blockIdx.x * blockDim.x + threadIdx.x;
	float total[] = {0,0};
	int count = 0;

        if(i < rows){

		for(int p = 0; p < rows; p++){

			if(assign[p]==i){
				for(int j = 0; j < cols; j++){
					total[j] = total[j] + points[(p*cols)+j];
				}

				count++;
			}

		}

	}

	for(int j = 0; j < cols; j++){
		centroids[(i*cols)+j] = total[j] / count;
	}

}

__global__ void compare(float *points, float *centroids, int *assign, int *assignnew, int k, int cols, int rows, int *dev_count){

	int i = blockIdx.x * blockDim.x + threadIdx.x;
	
	if(i < rows){
		if(assign[i] != assignnew[i]){
			*dev_count=*dev_count+1;
		}
	}
	
}

int main(int argc, char *argv[]){

	/*Declarations*/
	FILE *fp; /*declared file pointer*/
	char temp[512]; /*a temporary 512 character block to hold the line of the file*/
        int rows = 0;
        int cols = 0;

	fp = fopen(argv[1], "rb"); /*opens file*/

	/*check to see if it's all there*/
	if(fp==NULL){

		printf("Error opening file.\n");
		exit(1);

	}

	//counts lines in file and the first line to get number of columns (SOOO DIRTY!!!)
	while(fgets(temp, 512, fp) != NULL){
		if(rows==0){
			//how many columns?
			char * line;
			line = strtok(temp," ");
			while(line != NULL){
				line = strtok(NULL, " ");
				cols++;
			}
		} 
		rows++;
	}

	fclose(fp);

	fp = fopen(argv[1], "rb"); /*opens file*/

	printf("Array is %i x %i \n", rows, cols);

	//define array
	float *points = (float *) malloc(rows * cols * sizeof(float));
	if(points == NULL){
		printf("WHY!");
		exit(1);
	}
	
	int i = 0;
	int j = 0;

        //lets start adding them to the array
        while(fgets(temp, 512, fp) != NULL){

		j = 0;

		//get line
		char * line;
		line = strtok(temp," ");

		//split into columns
		while(line != NULL){
			float x = atoi(line);
			points[(i * cols) + j] = atof(line);
			line = strtok(NULL, " ");
			j++;
		}

		i++;

        }

	//randomly select K points
	int k = atoi(argv[2]);
	srand ( time(NULL) );
	float centroids[k*cols];

	for(int i = 0; i < k; i++){
		int random = rand() % rows;
		for(int j = 0; j < cols; j++){
			centroids[(i*cols)+j] = points[(random*cols)+j];
		}
	}

	//ok, lets have an array to assign which centoid it belongs to
	int assigned[rows];
	int assignednew[rows];

	float *dev_points, *dev_centroids;
	int* dev_assigned;
	int* dev_assignednew;
	
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(rows / dimBlock.x, rows / dimBlock.y);

	printf("Grid size = %i %i\n", (rows / dimBlock.x), (rows / dimBlock.y));

	//allocate memory to cuda
	cudaMalloc((void**)&dev_points, (rows*cols*sizeof(float))); //points
	cudaMalloc((void**)&dev_centroids, (k*cols*sizeof(float))); //centroids
	cudaMalloc((void**)&dev_assigned, (rows*sizeof(int))); //assignments
	cudaMalloc((void**)&dev_assignednew, (rows*sizeof(int))); //assignments new

	cudaMemcpy( dev_points, points, rows*cols*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy( dev_centroids, centroids, k*cols*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy( dev_assigned, assigned, rows*sizeof(int), cudaMemcpyHostToDevice);

	assign<<<512,512>>>(dev_points, dev_centroids, dev_assigned, k, cols, rows);

	//copy assigned back
	cudaMemcpy(assigned, dev_assigned, rows*sizeof(int), cudaMemcpyDeviceToHost); 

	int *dev_count;
	int host_count;

	//start loop
	do{

	        cudaMemcpy( dev_points, points, rows*cols*sizeof(float), cudaMemcpyHostToDevice);
        	cudaMemcpy( dev_centroids, centroids, k*cols*sizeof(float), cudaMemcpyHostToDevice);
	        cudaMemcpy( dev_assigned, assigned, rows*sizeof(int), cudaMemcpyHostToDevice);		

		//now that we have the assignments, i need to find the average for each centroid
		ave<<<1,10>>>(dev_points, dev_centroids, dev_assigned, k, cols, rows);	
	
		//copy centroids back
		cudaMemcpy(centroids, dev_centroids, k*cols*sizeof(float), cudaMemcpyDeviceToHost);

		//printing out centroids
		//for(int i = 0; i < k; i++){
		//	for(int c = 0; c < cols; c++){
		//		printf("%f ", centroids[(i*cols)+c]);
		//	}
		//	printf("\n");
		//}

		cudaMemcpy( dev_assignednew, assignednew, rows*sizeof(int), cudaMemcpyHostToDevice);

		assign<<<512,512>>>(dev_points, dev_centroids, dev_assignednew, k, cols, rows);

		cudaMemcpy(assignednew, dev_assignednew, rows*sizeof(int), cudaMemcpyDeviceToHost);

		host_count = 0;

		//compare to see if there is movement
		cudaMalloc( (void**)&dev_count, sizeof(int) );		
		cudaMemcpy( dev_count, &host_count, sizeof(int), cudaMemcpyHostToDevice );

		compare<<<512,512>>>(dev_points, dev_centroids, dev_assigned, dev_assignednew, k, cols, rows, dev_count);

		cudaMemcpy( &host_count, dev_count, sizeof(int), cudaMemcpyDeviceToHost );		

		memcpy(&assigned, assignednew, rows*sizeof(int) );

		printf("Number of changes %i\n", host_count);
	
	}while(host_count > 0);

	return EXIT_SUCCESS;
}

